#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

void write_headers_or_error(int client_socket, int status, size_t content_length, const char * restrict data) {
    char headers[2048];
    if(200 == status) {
        sprintf(headers, "HTTP/1.0 200 OK\r\n"
                        "Server: Simple HTTP Server\r\n"
                        "Content-Type: %s\r\n"
                        "Content-Length: %zu\r\n"
                        "\r\n",
                data, content_length);
    } else if(status==400) {
        sprintf(headers, "HTTP/1.0 400 Bad Request\r\n"
                            "\r\n"
                            "<h1>400 Bad Request</h1>",
                            "%s", data);
    } else if(status==404) {
        sprintf(headers, "HTTP/1.0 404 Not Found\r\n"
                           "\r\n"
                            "<h1>404 Not Found</h1>"
                );
    } else if(status==405) {
        sprintf(headers, "HTTP/1.0 405 Method Not Allowed\r\n");
    } else if(status==408) {
        sprintf(headers, "HTTP/1.0 408 Request Timeout\r\n"
                            "\r\n"
                            "<h1>408 Request Timeout</h1>");
    } else if(status==413) {
        sprintf(headers, "HTTP/1.0 413 Request Entity Too Large\r\n"
                            "\r\n"
                            "<h1>413 Request Entity Too Large</h1>");
    } else if(status==414) {
        sprintf(headers, "HTTP/1.0 414 Request-URL Too Long\r\n"
                            "\r\n"
                            "<h1>414 Request-URL Too Long</h1>");
    } else if(status==429) {
        sprintf(headers, "HTTP/1.0 429 Too Many Requests\r\n");
    } else if(status==500) {
        sprintf(headers, "HTTP/1.0 500 Internal Server Error\r\n"
                            "\r\n",
                            "500 Internal Server Error");
    } else if(status==501) {
        sprintf(headers, "HTTP/1.0 501 Not implemented\r\n"
                            "\r\n"
                            "<h1>501 Not implemented</h1>");
    } else if(status==505) {
        sprintf(headers, "HTTP/1.0 505 Http Version Not Supported\r\n"
                            "\r\n"
                            "505 Http Version Not Supported");
    }

    write(client_socket, headers, strlen(headers));
};
