#include "parser.h"
#include "generate.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <stdint.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>


enum command_t{
	CMD_UNKNOWN = -1,
	CMD_GET = 1,
	CMD_POST = 2
};

void interact_connection(int client_socket, const char * restrict client_ip, const uint16_t client_port,
						 const char * restrict htdocs_dir, const char * cgi_dir);
void do_get(int client_socket, const char * restrict url, const char * query_string, const char * restrict client_ip,
            const uint16_t client_port, const char * restrict htdocs_dir, const char * cgi_dir);
void do_post(int client_socket, const char * restrict url, const char * restrict client_ip,
             const uint16_t client_port, const char * restrict htdocs_dir, const char * cgi_dir, int* conent_length);

size_t get_file_size(char * filename) {
	struct stat st;
	stat(filename, &st);
	return st.st_size;
}

void process_connection(int client_socket, const struct sockaddr_in * restrict client_address, const char * restrict htdocs_dir, const char * cgi_dir) {
	char client_ip[INET_ADDRSTRLEN];

	const struct in_addr client_ip_addr = client_address->sin_addr;
	inet_ntop(AF_INET, &(client_ip_addr), client_ip, sizeof(client_ip));

	const uint16_t client_port = ntohs(client_address->sin_port);

	pid_t pid = fork();
	if(pid == 0) {
		interact_connection(client_socket, client_ip, client_port, htdocs_dir, cgi_dir);
        exit(0);
	} else {
		close(client_socket);
	}
}

volatile int p=0;

void handlerTimeOut(int sig) {
    kill(p, SIGKILL);
    exit(0);
}

void interact_connection(int client_socket, const char * restrict client_ip, const uint16_t client_port,
						 const char * restrict htdocs_dir, const char * cgi_dir) {

    signal(SIGALRM, handlerTimeOut);

    struct itimerval to;
    // TIMEOUT 30 seconds.
    to.it_value.tv_sec = 30;
    to.it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, &to, NULL);

    //printf("%d\n", getpid());

	char recv_buffer[16536]; memset(recv_buffer, 0, sizeof(recv_buffer));
	ssize_t bytes_received;
	char command_arg[2000]; memset(command_arg, 0, sizeof(command_arg));
    char query_string[2000]; memset(query_string, 0, sizeof(query_string));

	enum command_t command = CMD_UNKNOWN;
    int content_length = 0;


	// read HTTP query from client while not 2 enter
	int cnt_bytes = 0, cur_cnt=0;
	int flag = 1;
	while(flag) {
		cur_cnt = read(client_socket, recv_buffer+cnt_bytes, sizeof(recv_buffer)-cnt_bytes);

		int start = cnt_bytes - 2;
		if(start < 0) start = 0;
		for(int i = start; i < cnt_bytes+cur_cnt-3; i++) {
			if(recv_buffer[i]==13 && recv_buffer[i+1]==10 && recv_buffer[i+2]==13 && recv_buffer[i+3]==10) {
				flag=0;break;
			}
		}
		cnt_bytes+=cur_cnt;
	}
	bytes_received = cnt_bytes;

    double version;
    int syntax_error = 0;
	parse_command(recv_buffer, &command, command_arg, query_string, &content_length, sizeof(command_arg), &version, &syntax_error);

    if(syntax_error) {
        write_headers_or_error(client_socket, 400, 0, NULL);
        return;
    }

    if(content_length > 65536) {
        write_headers_or_error(client_socket, 413, 0, NULL);
        return;
    }

    if(strlen(command_arg) > 1000) {
        write_headers_or_error(client_socket, 414, 0, NULL);
        return;
    }

    if(version==0) {
        write_headers_or_error(client_socket, 505, 0, NULL);
        return;
    }

	if(CMD_GET == command) {
		do_get(client_socket, command_arg, query_string, client_ip, client_port, htdocs_dir, cgi_dir);
	} else if (CMD_POST == command) {
		do_post(client_socket, command_arg, client_ip, client_port, htdocs_dir, cgi_dir, &content_length);
	} else {
        write_headers_or_error(client_socket, 501, 0, NULL);
    }
}

void do_get(int client_socket, const char * restrict url, const char * query_string, const char * restrict client_ip,
			const uint16_t client_port, const char * restrict htdocs_dir, const char * cgi_dir) {

	char script_file_name[2048]; memcpy(script_file_name, cgi_dir, strlen(htdocs_dir));
	if(0 == strcmp("/", url) || 0==strlen(url)) {
		strcat(script_file_name, "/index");
	} else {
		strcat(script_file_name, url);
	}

    p = fork();
	if(!p) {
        //signal(SIGKILL, handlerTimeOut2);
		dup2(client_socket,1);
		close(client_socket);

        setenv("SERVER_SOFTWARE", "Simple server v.1", 1);
        setenv("SERVER_PROTOCOL", "HTTP", 1);
        setenv("SERVER_PORT", "50000", 1);
        setenv("REQUEST_METHOD", "GET", 1);
        setenv("PATH_INFO", url , 1);
        setenv("QUERY_STRING", query_string, 1);

        execlp(script_file_name, script_file_name, NULL);

         _exit(1);
	}

	int status;
	wait(&status);
	if(WIFEXITED(status) && WEXITSTATUS(status) != 0) {
		char static_file_name[2048]; memcpy(static_file_name, htdocs_dir, strlen(htdocs_dir));
		if(0 == strcmp("/", url) || 0==strlen(url)) {
			strcat(static_file_name, "/index.html");
		} else {
			strcat(static_file_name, url);
		}

		int f_id = open(static_file_name, O_RDONLY);
		if(-1 == f_id) {
			//404
			write_headers_or_error(client_socket, 404, 0, "Not Found");
		} else {
			//200
			write_headers_or_error(client_socket, 200, get_file_size(static_file_name), "mime type");
			char file_buffer[65536];
			while(1) {
				ssize_t bytes_read = read(f_id, file_buffer, sizeof(file_buffer));
				if(bytes_read > 0) {
					ssize_t bytes_written = write(client_socket, file_buffer, bytes_read);
				} else {
					break;
				}
			}
			close(f_id);
		}
	}
};

void do_post(int client_socket, const char * restrict url, const char * restrict client_ip,
             const uint16_t client_port, const char * restrict htdocs_dir, const char * cgi_dir, int* content_length) {

    char script_file_name[2048]; memcpy(script_file_name, cgi_dir, strlen(htdocs_dir));
    if(0 == strcmp("/", url) || 0==strlen(url)) {
        strcat(script_file_name, "/index");
    } else {
        strcat(script_file_name, url);
    }

    p = fork();
    if(!p) {
        dup2(client_socket,1);
        dup2(client_socket, 0);
        close(client_socket);


        setenv("SERVER_SOFTWARE", "Simple server v.1", 1);
        setenv("SERVER_PROTOCOL", "HTTP", 1);
        setenv("SERVER_PORT", "50000", 1);
        setenv("REQUEST_METHOD", "POST", 1);
        setenv("PATH_INFO", url , 1);
        setenv("CONTENT_LENGTH", content_length, 1);

        execlp(script_file_name, script_file_name, NULL);

        _exit(1);
    }

    int status;
    wait(&status);
    if(WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        //404
        write_headers_or_error(client_socket, 404, 0, "Not Found");

    }

}