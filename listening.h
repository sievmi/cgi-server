#ifndef LISTENING_H
#define LISTENING_H

#include <sys/types.h>
#include <stdint.h>

void  start_listening(const uint16_t port_no, const char * restrict htdocs_dir, const char * cgi_dir);

#endif 
