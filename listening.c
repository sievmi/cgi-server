#include "process.h"
#include <sys/socket.h>
#include <stdint.h>
#include <stdio.h>
#include <arpa/inet.h>

void start_listening(const uint16_t port_no, const char * restrict htdocs_dir, const char * cgi_dir) {
	int server_socket = socket(AF_INET, SOCK_STREAM, 0); // Создаем TCP-сокет
	struct sockaddr_in server_address;
	server_address.sin_addr.s_addr = INADDR_ANY;
	server_address.sin_family = AF_INET;

	server_address.sin_port = htons(port_no);

    int sopt = 1;
    setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &sopt, sizeof(sopt));

	bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address));
	listen(server_socket, SOMAXCONN);

	int client_socket;
	struct sockaddr_in client_address;
	socklen_t client_address_size = sizeof(client_address);
	while(1) {
		client_socket = accept(server_socket, (struct sockaddr*) &client_address, &client_address_size);
		process_connection(client_socket, &client_address, htdocs_dir, cgi_dir);
	}
}
