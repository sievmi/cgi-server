#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

enum command_t{
	CMD_UNKNOWN = -1,
	CMD_GET = 1,
	CMD_POST = 2
};

void parse_command(char * recv_buffer, enum command_t * command, char * command_arg, char * query_string,
                   int* content_length, ssize_t command_arg_size, double* version, int* syntax_error) {
	if(sizeof(recv_buffer) < 3) {
		*syntax_error = 1;
        return;
	}
	char sub_buf[5];
	memcpy(sub_buf, recv_buffer, 3);
	sub_buf[3] = '\0';
	if(strncmp(sub_buf, "GET", 3) == 0) {
		*command = CMD_GET;
	}
	
	if(sizeof(recv_buffer) >= 4) {
		memcpy(sub_buf, recv_buffer, 4);
		sub_buf[4] = '\0';
		if(strncmp(sub_buf, "POST", 4) == 0) {
			*command = CMD_POST;
		}
	}  
	
	if(*command == CMD_UNKNOWN) {
        if(!isalpha(*recv_buffer)) {
            *syntax_error = 1;
        }
        return;
	}

	
	char* start = recv_buffer;
	while(*start && *start != '/') {
		start++;
	}

	char* t = start;

	if(*start == '/') {
		char* end = start;
		while(*end && *end != ' ') {
			if(*end == '?')
				t = end;
			end++;
		}
		if(*end == ' ') {
			if(*t == '?') {
				memcpy(query_string, t+1, (end-t));
				memcpy(command_arg, start, (t - start));
			} else {
				memcpy(command_arg, start, (end - start));
			}

		}

	}

    if(*command==CMD_POST) {
        char* pos = strstr(start, "Content-Length:");
        if(pos!=NULL) {
            char* start_pos = pos+strlen("Content-Length");
            while(*start_pos==' ') start_pos++;
            if(*start_pos==':') {
                start_pos++;
            } else {
                *syntax_error = 1;
                return;
            }
            while(*start_pos==' ') start_pos++;
            char* end_pos = start_pos;
            while(isdigit(*end_pos)) end_pos++;
            char buff[30];
            memcpy(buff, start_pos, (end_pos-start_pos));
            *content_length = strtol(start_pos, end_pos, 10);
        }
    }

    char* pos = strstr(start, "HTTP/");
    *version = 0;
    if(pos!=NULL) {
        pos = pos + strlen("HTTP/");
        char * end = pos + 2;
        *version = strtold(pos, &end);
    } else {
        *syntax_error = 1;
    }
    if(*version!=1.0 && *version!=1.1)
        *version=0;


    if(*syntax_error==1 || *version==0) return;

    pos = pos+5;
    int cnt = 0;
    int i=0;
    while(*pos != '\0') {
        i++;
        char* prev = pos-1;
        char* pprev = pos-2;
        char* ppprev = pos - 3;
        char* next = pos+1;
        // т.к. допускается передача значения в нескольких строках, ессли следующая начинается с пробела.
        if(*pos=='\n' && *prev=='\r' && !(*ppprev=='\r' && *pprev=='\n') && *next!=' ') {
            if(cnt < 1) {
                *syntax_error = 1;
                return;
            } else {
                cnt = 0;
            }
        } else if(*pos==':')
            cnt++;
        pos++;
    }

};

