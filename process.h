#include "parser.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <stdint.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <stdlib.h>



void interact_connetcion(int client_socket, const char * restrict client_ip, const uint16_t client_port,
                         const char * restrict htdocs_dir, const char * cgi_dir);
void do_get(int client_socket, const char * restrict url, const char * restrict client_ip,
            const uint16_t client_port, const char * restrict htdocs_dir, const char * cgi_dir);
void do_post(int client_socket, const char * restrict url, const char * restrict client_ip,
             const uint16_t client_port, const char * restrict htdocs_dir, const char * cgi_dir, int* conent_length);
void process_connection(int client_socket, const struct sockaddr_in * restrict client_address,
                        const char * restrict htdocs_dir, const char * cgi_dir);
size_t get_file_size(char * filename);
