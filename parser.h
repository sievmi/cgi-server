#include <sys/types.h>

enum command_t;

void parse_command(char * recv_buffer, enum command_t * command, char * command_arg, char * query_string,
                   int* content_length, ssize_t command_arg_size, double* version, int* syntax_error);
